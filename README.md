# OpenML dataset: FIFA20-Players-Dataset-with-Stats--Images

https://www.openml.org/d/43766

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

About Dataset
This data set includes15K Fifa20 Players with 15+ features and their images , including their position, age, and Country, and many more. It can be used for learning Statistics, Performing Data Analysis, and Data Visualization using various libraries like Seaborn, Pandas-Bokeh, and Plotly. It can be used to plot various Plots to understand the comparison between various features.
References

Sofifa.com

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43766) of an [OpenML dataset](https://www.openml.org/d/43766). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43766/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43766/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43766/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

